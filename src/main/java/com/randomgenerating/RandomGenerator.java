package com.randomgenerating;

import org.apache.commons.lang3.RandomStringUtils;
import java.util.concurrent.ThreadLocalRandom;

public class RandomGenerator {
    private RandomGenerator() {}
    public static String getRandomString(int min, int max) {
        int length = ThreadLocalRandom.current().nextInt(min, max);
        return RandomStringUtils.randomAlphabetic(length);
    }
    public static int getRandomInt(int min, int max){
      return ThreadLocalRandom.current().nextInt(min, max);

    }
}


package com;

import com.connection.PooledDataSource;
import com.data.GetResultFromSelect;
import com.data.TableDataLoader;
import com.logging.LoggerManager;
import com.utils.ddl.DDLCreationExecutor;
import com.utils.LoadProperties;
import com.validate.ValidateProduct;
import lombok.SneakyThrows;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.slf4j.Logger;

import java.io.FileReader;
import java.sql.Connection;

public class Main {
    private static final Logger LOGGER = LoggerManager.getLogger();
    private static final String CONFIG = "config.properties";
    private static final String BATCH_SIZE = "batch_size";
    private static final String STORE_CSV_FILE_NAME = "storeCSVPath";
    private static final String PRODUCT_TYPE_CSV_FILE_NAME = "productTypeCSVPath";
    private static final  String TYPE = "type";
    static Connection connection;
    static LoadProperties properties;
   static boolean createDDLTablesAndIndexes = true;

    @SneakyThrows
    public static void main(String[] args) {

        properties = new LoadProperties(CONFIG);
        LOGGER.info("Start Load Property {} from {}", CONFIG, Main.class.getName());
        properties.loadProperties();
        LOGGER.info("Finish Load Property {} from {}", CONFIG, Main.class.getName());

        switcherCreateTablesAndIndexes(args);

        PooledDataSource pooledDataSource = PooledDataSource.getInstance();

        DDLCreationExecutor tableCreation = new DDLCreationExecutor();

        connection = pooledDataSource.getConnection();

        int batchSize = Integer.parseInt(properties.getProperty(BATCH_SIZE));

        var generalStart = System.nanoTime();
        LOGGER.info("The main method start: from {}", Main.class.getName());

        if (createDDLTablesAndIndexes) {
            boolean ddlTablesCreated = tableCreation.createDDLTables(properties);
            if (!ddlTablesCreated) {
                LOGGER.error("Failed to create DDL tables. Exiting...");
                return;
            }
        }

        TableDataLoader dataLoader = new TableDataLoader(new ValidateProduct(), properties, batchSize);

        try (var connection = Main.connection;
             CSVParser csvStore = new CSVParser(new FileReader(properties.getProperty(STORE_CSV_FILE_NAME)),
                     CSVFormat.DEFAULT.withHeader());
             CSVParser csvProductType = new CSVParser(
                     new FileReader(properties.getProperty(PRODUCT_TYPE_CSV_FILE_NAME)),
                     CSVFormat.DEFAULT.withHeader())) {

            if (createDDLTablesAndIndexes) {
                dataLoader.fillStoreTable(connection, csvStore);
                dataLoader.fillProductTypeTable(connection,csvProductType);
                dataLoader.fillProductTable(connection);
                dataLoader.fillRemainTable(connection);
                boolean ddlIndexesAdded = tableCreation.addIndex(properties);
                if (!ddlIndexesAdded) {
                    LOGGER.error("Failed to add indexes. Exiting...");
                    return;
                }
            }

            var startSelect = System.nanoTime();
            GetResultFromSelect resultSet = new GetResultFromSelect();
            resultSet.getResultFromSelect(Main.connection, returnDType());
            var endSelect = System.nanoTime();
            var resultCountSelect = (endSelect - startSelect) / 1_000_000;
            LOGGER.info("Searching total amount of products of one Type taken: {} millis", resultCountSelect);

            var generalEnd = System.nanoTime();
            var totalAmountOfWork = (generalEnd - generalStart) / 1_000_000;
            LOGGER.info("General work of the program took: {} millis", totalAmountOfWork);
        }
    }

    private static void switcherCreateTablesAndIndexes(String[] args) {
        if (args.length == 1) {
            String bool= args[0];
            if (bool.equals("true") || bool.equals("false")) {
                createDDLTablesAndIndexes = Boolean.parseBoolean(bool);
            }
        }
    }

    public static String returnDType() {
        return System.getProperty(TYPE, "мед");
    }
}

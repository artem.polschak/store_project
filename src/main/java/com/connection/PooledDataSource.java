package com.connection;

import com.logging.LoggerManager;
import com.utils.LoadProperties;
import lombok.SneakyThrows;
import org.postgresql.ds.PGSimpleDataSource;
import org.slf4j.Logger;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedDeque;

public class PooledDataSource extends PGSimpleDataSource {
    private static final Logger LOGGER = LoggerManager.getLogger();
    private Queue<Connection> connectionPool;
    private static final String LOGIN = "login";
    private static final String PASSWORD = "password";
    private static final String CONNECTION = "connection";
    private static final String CONFIG = "connection.properties";
    private static final String POOL_SIZE = "pool_size";
    private static PooledDataSource instance;

    public static synchronized PooledDataSource getInstance() {
        if (instance == null) {
            instance = new PooledDataSource();
        }
        return instance;
    }

    @SneakyThrows
    private PooledDataSource() {
        super();
        createConnection();
    }

    public void createConnection() throws SQLException {
        LoadProperties properties = loadProperty();
        int poolSize = getPoolSize(properties);
        setConnectionConfiguration(properties);
        connectionPool = createPoolConnections(poolSize);

        LOGGER.info("Created new Connection Pool with {} active connections successfully from {}", poolSize, PooledDataSource.class.getName());
    }

    LoadProperties loadProperty() {
        LoadProperties properties = new LoadProperties(CONFIG);
        LOGGER.info("Start Load Property {} from {}",CONFIG, PooledDataSource.class.getName());
        properties.loadProperties();
        LOGGER.info("Finish Load Property {} from {}",CONFIG, PooledDataSource.class.getName());
        return properties;
    }

     int getPoolSize(LoadProperties properties) {
        return Integer.parseInt(properties.getProperty(POOL_SIZE));
    }

    Queue<Connection> createPoolConnections(int poolSize) throws SQLException {
        Queue<Connection> connections =  new ConcurrentLinkedDeque<>();
        for (int i = 0; i < poolSize; i++) {
            var physicalConnection = super.getConnection();
            var connection = new ConnectionProxy(physicalConnection, connections);
            connections.add(connection);
        }
        return connections;
    }
    void setConnectionConfiguration(LoadProperties properties) {
        setURL(properties.getProperty(CONNECTION));
        setUser(properties.getProperty(LOGIN));
        setPassword(properties.getProperty(PASSWORD));
    }

    public int getConnectionPoolSize() {
        return connectionPool.size();
    }

    @Override
    public Connection getConnection() {
        return connectionPool.poll();
    }

    public void setConnectionPool(Queue<Connection> connectionPool) {
        this.connectionPool.clear();
        this.connectionPool.addAll(connectionPool);
    }
}

package com.validate;

import com.model.DTO;
import jakarta.validation.ConstraintViolation;
import jakarta.validation.Validation;
import jakarta.validation.Validator;
import jakarta.validation.ValidatorFactory;
import java.util.Set;

public class ValidateProduct {
    Validator validator;
    ValidatorFactory validatorFactory;

    void createValidatorFactory() {
        validatorFactory = Validation.buildDefaultValidatorFactory();
        validator = validatorFactory.getValidator();
    }
    public boolean validate(DTO object) {
        createValidatorFactory();
        Set<ConstraintViolation<DTO>> validate = validator.validate(object);
        return validate.isEmpty();
    }
}



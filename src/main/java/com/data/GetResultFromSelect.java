package com.data;

import com.logging.LoggerManager;
import org.slf4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
public class GetResultFromSelect {
    private static final Logger LOGGER = LoggerManager.getLogger();

    public void getResultFromSelect(Connection dataConnection, String productTypeName) {
        try (Connection connection = dataConnection) {
            String sql = "SELECT s.name AS store, s.location AS address, pt.name AS product, SUM(r.amount) AS total_amount " +
                    "FROM store s " +
                    "JOIN remain r ON s.id = r.store_id " +
                    "JOIN product p ON p.id = r.product_id " +
                    "JOIN product_type pt ON pt.id = p.product_type_id " +
                    "WHERE pt.name = ? " +
                    "GROUP BY s.name, s.location, pt.name " +
                    "ORDER BY total_amount DESC " +
                    "LIMIT 1";

            ResultSet resultSet;
            try (PreparedStatement statement = connection.prepareStatement(sql)) {
                statement.setString(1, productTypeName);
                resultSet = statement.executeQuery();

                while (resultSet.next()) {
                    String storeName = resultSet.getString("store");
                    String location = resultSet.getString("address");
                    String productType = resultSet.getString("product");
                    int totalAmount = resultSet.getInt("total_amount");
                    LOGGER.info("крамниця:{}", storeName);
                    LOGGER.info("адреса:{}", location);
                    LOGGER.info("тип продукту:{}", productType);
                    LOGGER.info("загальна кількість:{}", totalAmount);
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}



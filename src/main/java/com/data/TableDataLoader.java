package com.data;

import com.logging.LoggerManager;
import com.model.ProductDTO;
import com.model.ProductTypeDTO;
import com.model.StoreDTO;
import com.utils.LoadProperties;
import com.validate.ValidateProduct;
import lombok.SneakyThrows;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.slf4j.Logger;

import java.sql.Connection;

import static com.randomgenerating.RandomGenerator.getRandomInt;
import static com.randomgenerating.RandomGenerator.getRandomString;

public class TableDataLoader {
    private static final Logger LOGGER = LoggerManager.getLogger();
    private static final String COUNT_VALID_PRODUCT = "count_valid_product";
    private static final String MIN_PRODUCT_AMOUNT_GENERATE = "min_product_amount_generate";
    private static final String MAX_PRODUCT_AMOUNT_GENERATE = "max_product_amount_generate";
    private static final String REMAIN_AMOUNT = "remain_amount";
    int productTypeSize;
    int storeSize;
    int countValidProduct;
    int countInvalidProduct;
    int batchSize;
    ValidateProduct validateProduct;
    LoadProperties properties;


    public TableDataLoader(ValidateProduct validateProduct, LoadProperties properties, int batchSize) {
        this.validateProduct = validateProduct;
        this.properties = properties;
        this.batchSize = batchSize;
    }

    @SneakyThrows
    public void fillStoreTable(Connection connection, CSVParser csvStore) {
        LOGGER.info("Begin filling table store");
        var startStore = System.nanoTime();

        try (var statement = connection.prepareStatement("insert into store(name, location) values(?, ?);")) {
            String name;
            String location;
            for (CSVRecord writer : csvStore) {
                name = writer.get("name");
                location = writer.get("location");

                if (validateProduct.validate(new StoreDTO(name, location))) {
                    statement.setString(1, name);
                    statement.setString(2, location);
                    statement.executeUpdate();
                    storeSize++;
                }
            }
        }
        var endStore = System.nanoTime();
        var resultStore = (endStore - startStore) / 1_000_000;
        LOGGER.info("filling the table store took: {} millis", resultStore);
    }

    @SneakyThrows
    public void fillProductTypeTable(Connection connection, CSVParser csvProductType) {
        LOGGER.info("Begin filling table product_type ");
        var startProductType = System.nanoTime();
        try (var statement = connection.prepareStatement(
                "insert into product_type(name) values(?);")) {
            String productType;
            for (CSVRecord writer : csvProductType) {
                productType = writer.get("name");

                if (validateProduct.validate(new ProductTypeDTO(productType))) {
                    statement.setString(1, productType);
                    statement.executeUpdate();
                    productTypeSize++;
                }
            }
        }
        var endProductType = System.nanoTime();
        var resultProductType = (endProductType - startProductType) / 1_000_000;
        LOGGER.info("filling the table product_type took: {} millis", resultProductType);

    }

    @SneakyThrows
    public void fillProductTable(Connection connection) {
        LOGGER.info("Begin filling table product");
        var startProduct = System.nanoTime();
        try (var insertStatement = connection.prepareStatement(
                "insert into product(name, product_type_id) values(?, ?);")) {
            int count = 1;
            int productAmount = Integer.parseInt(properties.getProperty(COUNT_VALID_PRODUCT));
            connection.setAutoCommit(false);
            while (count <= productAmount) {
                int result = getRandomInt(1, productTypeSize);
                String product = getRandomString(1, 30);
                if (validateProduct.validate(new ProductDTO(product))) {
                    insertStatement.setString(1, product);
                    insertStatement.setInt(2, result);
                    insertStatement.addBatch();
                    countValidProduct++;
                    count++;
                    if (count % batchSize == 0) {
                        LOGGER.info("Batch work {}", insertStatement.executeBatch().length);
                        insertStatement.executeBatch();
                        connection.commit();
                    }
                } else {
                    countInvalidProduct++;
                }
            }
            insertStatement.executeBatch();
            connection.commit();
            connection.setAutoCommit(true);
        }
        var endProduct = System.nanoTime();
        var resultProduct = (endProduct - startProduct) / 1_000_000;
        LOGGER.info("Count valid product = {}", countValidProduct);
        LOGGER.info("count invalid product = {}", countInvalidProduct);
        LOGGER.info("filling the table product took: {} millis", resultProduct);
    }

    @SneakyThrows
    public void fillRemainTable(Connection connection) {

        LOGGER.info("Begin filling table amount");
        var startRemain = System.nanoTime();

        int minAmount = Integer.parseInt(properties.getProperty(MIN_PRODUCT_AMOUNT_GENERATE));
        int maxAmount = Integer.parseInt(properties.getProperty(MAX_PRODUCT_AMOUNT_GENERATE));
        int maxProducts = Integer.parseInt(properties.getProperty(REMAIN_AMOUNT));

        try (var statement = connection.prepareStatement(
                "insert into remain(amount, product_id, store_id) values(?, ?, ?);")) {
            connection.setAutoCommit(false);
            for (int i = 1; i <= maxProducts; i++) {
                int amount = getRandomInt(minAmount, maxAmount);
                int product = getRandomInt(1, countValidProduct);
                int store = getRandomInt(1, storeSize);

                statement.setInt(1, amount);
                statement.setInt(2, product);
                statement.setInt(3, store);

                statement.addBatch();

                if (i % batchSize == 0) {
                    LOGGER.debug("Butch work {}", statement.executeBatch().length);
                    statement.executeBatch();
                    connection.commit();
                }
            }
            statement.executeBatch();
            connection.commit();
            connection.setAutoCommit(true);
        }
        var endRemain = System.nanoTime();
        var resultRemain = (endRemain - startRemain) / 1_000_000;
        LOGGER.info("filling {} products in the table amount took: {} millis",maxProducts, resultRemain);
        var result = maxProducts / (resultRemain / 1000);
        LOGGER.info("RPS generating products = {}", result);
    }
}

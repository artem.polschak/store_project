package com.utils.ddl;

import com.connection.PooledDataSource;
import com.logging.LoggerManager;
import com.utils.LoadProperties;
import lombok.SneakyThrows;
import org.slf4j.Logger;
import java.io.BufferedReader;
import java.io.FileReader;
import java.sql.Connection;

public class DDLCreationExecutor {
    private static final Logger LOGGER = LoggerManager.getLogger();
    private static final String DDL_SCHEMA = "DDL_schema";
    private static final String DDL_INDEX = "DDL_add_index";
    PooledDataSource pooledDataSource;
    Connection dataConnection;

    public DDLCreationExecutor() {
        pooledDataSource = PooledDataSource.getInstance();
        dataConnection = pooledDataSource.getConnection();
    }

    @SneakyThrows
    void executeSqlScript(Connection connection, String sqlScriptPath) {
        try (var statement = connection.createStatement();
             var reader = new BufferedReader(new FileReader(sqlScriptPath))) {
            LOGGER.info("Executing SQL script {} from {}", sqlScriptPath, DDLCreationExecutor.class.getName());

            String line;
            StringBuilder script = new StringBuilder();
            while ((line = reader.readLine()) != null) {
                script.append(line);

                if (line.trim().endsWith(";")) {
                    String sqlStatement = script.toString();
                    statement.execute(sqlStatement);
                    script.setLength(0);
                }
            }
        }
    }

    boolean executeOperation(String sqlScript, String operationName) {
        try (var connection = dataConnection) {
            LOGGER.info("Opened new connection to the database from {} for executing {}",
                    DDLCreationExecutor.class.getName(), sqlScript);
            executeSqlScript(connection, sqlScript);
            LOGGER.info("{} completed successfully: {}", operationName, sqlScript);
            LOGGER.info("Connection closed {}", DDLCreationExecutor.class.getName());
            return true;
        } catch (Exception e) {
            LOGGER.error("Error {}: {} {}", operationName, e.getMessage(), e);
            return false;
        }
    }

    public boolean createDDLTables(LoadProperties properties) {
        String sqlScript = properties.getProperty(DDL_SCHEMA);
        return executeOperation(sqlScript, "creating DDL tables");
    }

    public boolean addIndex(LoadProperties properties) {
        String sqlScript = properties.getProperty(DDL_INDEX);
        return executeOperation(sqlScript, "adding indexes");
    }
}

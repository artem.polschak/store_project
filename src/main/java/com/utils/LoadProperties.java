package com.utils;

import com.logging.LoggerManager;
import org.slf4j.Logger;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.Objects;
import java.util.Properties;

public class LoadProperties {
    private static final Logger LOGGER = LoggerManager.getLogger();
    private final String config;
    private Properties properties;

    public LoadProperties(String config) {
        this.config = config;
    }

    public String getProperty(String key) {
        return properties.getProperty(key);
    }

    public void setProperties(String key, String value) {
        this.properties.setProperty(key, value);
    }

    String getPathProperty(String fileName)  {
        File file = new File((Objects.requireNonNull(getClass().getClassLoader().getResource(fileName))).getFile());
        return file.getParent() + File.separator;
    }

    public void loadProperties() {
        properties = new Properties();

        String path = getPathProperty(config);

        LOGGER.info("Loading {} from location {}", config, path);

        InputStream inputStream = getClass().getClassLoader().getResourceAsStream(config);
        try (InputStreamReader reader = new InputStreamReader(Objects.requireNonNull(inputStream), StandardCharsets.UTF_8)) {
            properties.load(reader);
        } catch (IOException e) {
            LOGGER.error("Failed to load {} from path:{}, {}, {}", config, path, e.getMessage(), e);
        }
    }
}

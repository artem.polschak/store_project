package com.model;

import jakarta.validation.constraints.*;

public class ProductTypeDTO implements DTO {
    @NotNull(message = "Name is compulsory")
    @NotBlank(message = "Name is compulsory")
    @Size(min = 3, message = "Size must have minimum two letters")
    String productType;

    public ProductTypeDTO(String product) {
        this.productType = product;
    }

}

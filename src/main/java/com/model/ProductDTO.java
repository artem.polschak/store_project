package com.model;

import jakarta.validation.constraints.*;

public class ProductDTO implements DTO {
    @NotNull(message = "Name is compulsory")
    @NotBlank(message = "Name is compulsory")
    @Size(min = 2, message = "Size must have minimum two letters")
    String product;

    public ProductDTO(String product) {
        this.product = product;
    }

}

package com.model;

import jakarta.validation.constraints.*;

public class StoreDTO implements DTO {
    @NotNull(message = "Name is compulsory")
    @NotBlank(message = "Name is compulsory")
    @Size(min = 1, max = 30, message = "Size must have minimum two letters")
    String store;
    @NotNull(message = "Name is compulsory")
    @NotBlank(message = "Name is compulsory")
    @Size(min = 1, max = 40, message = "Size must have minimum two letters")
    String location;

    public StoreDTO(String store, String location) {
        this.store = store;
        this.location = location;
    }
}

package com.connection;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Queue;

import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class ConnectionProxyTest {
    @Mock
    Connection physicalConnection;

    @Mock
    Queue<Connection> connectionPool;

    @Test
    void testClose() throws SQLException {
        ConnectionProxy connectionProxy = new ConnectionProxy(physicalConnection, connectionPool);
        connectionProxy.close();
        verify(connectionPool, times(1)).add(connectionProxy);
        verify(physicalConnection, never()).close();
    }
}
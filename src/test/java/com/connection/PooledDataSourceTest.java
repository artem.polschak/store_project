package com.connection;

import com.utils.LoadProperties;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.*;
import org.mockito.junit.jupiter.MockitoExtension;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Queue;

import static org.assertj.core.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.sql.Connection;
import java.util.Queue;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class PooledDataSourceTest {
   // @Mock
   // private PGSimpleDataSource dataSourceMock;

    @Mock
    private Connection connectionMock;

    private PooledDataSource pooledDataSource;

    @BeforeEach
    public void setup() {

        pooledDataSource = PooledDataSource.getInstance();
        pooledDataSource.setConnectionPool(mock(Queue.class));
        pooledDataSource.setURL("jdbc:mysql://localhost:3306/test");
        pooledDataSource.setUser("username");
        pooledDataSource.setPassword("password");
    }

    @Test
    public void testGetConnectionPoolSize() {
        LoadProperties properties = mock(LoadProperties.class);
        properties.setProperties("pool_size", "2");


       /* Connection connection = mock(Connection.class);
        PreparedStatement statement = mock(PreparedStatement.class);

        int location = 1;
        int product = 2;
        int remain = 3;
        int maxSumOfQuantity = 4;
        int sizeBatch = 100;
        int sizeOfMulti = 3;*/

       // when(properties.getProperty("count_valid_product")).thenReturn(String.valueOf(product));
        when(pooledDataSource.loadProperty()).thenReturn(properties);
        when(properties.getProperty("pool_size")).thenReturn(String.valueOf(2));
        when(pooledDataSource.getPoolSize(properties)).thenReturn(2);


        // Mocking the behavior
        Queue<Connection> connectionPool = mock(Queue.class);
        when(connectionPool.size()).thenReturn(5);
        pooledDataSource.setConnectionPool(connectionPool);

        // Calling the method under test
        int size = pooledDataSource.getConnectionPoolSize();

        // Verifying the expected behavior
        assertEquals(5, size);
        verify(connectionPool).size();
    }

    @Test
    public void testGetConnection() {
        // Mocking the behavior
        Queue<Connection> connectionPool = mock(Queue.class);
        when(connectionPool.poll()).thenReturn(connectionMock);
        pooledDataSource.setConnectionPool(connectionPool);

        // Calling the method under test
        Connection connection = pooledDataSource.getConnection();

        // Verifying the expected behavior
        assertEquals(connectionMock, connection);
        verify(connectionPool).poll();
    }

    // Add more test cases as needed for other methods and scenarios

}
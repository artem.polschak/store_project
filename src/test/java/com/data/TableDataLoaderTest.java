package com.data;

import com.utils.LoadProperties;
import com.validate.ValidateProduct;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import static org.assertj.core.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.powermock.api.mockito.PowerMockito.when;

@ExtendWith(MockitoExtension.class)
class TableDataLoaderTest {

    @Mock
    ValidateProduct validateProduct;
    @Mock
    LoadProperties properties;

    TableDataLoader loader;
    @Mock
    Connection connection;

    CSVParser parser;

    @Mock
    PreparedStatement statement;


    @Test
    void fillStoreTable() throws IOException, SQLException {
        loader = new TableDataLoader(validateProduct, properties, 10);
        parser = new CSVParser(new FileReader("src/test/resources/store.csv"),
                CSVFormat.DEFAULT.withHeader());

        loader.fillStoreTable(connection, parser);
        when(connection.prepareStatement(anyString())).thenReturn(statement);
        verify(statement, times(2)).setString(anyInt(), anyString());

    }
}
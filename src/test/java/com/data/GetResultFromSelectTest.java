package com.data;


import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.Mock;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;


import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class GetResultFromSelectTest {
    @Mock
    private Connection connection;

    @Mock
    private PreparedStatement statement;

    @Mock
    private ResultSet resulSet;

    @Test
     void testGetResultFromSelect() throws SQLException {
        GetResultFromSelect getResultFromSelect = new GetResultFromSelect();

        when(connection.prepareStatement(anyString())).thenReturn(statement);
        when(statement.executeQuery()).thenReturn(resulSet);
        when(resulSet.next()).thenReturn(true, false);

        getResultFromSelect.getResultFromSelect(connection, "ProductType");

        verify(resulSet, times(3)).getString(anyString());
        verify(resulSet).getInt(anyString());
    }
}
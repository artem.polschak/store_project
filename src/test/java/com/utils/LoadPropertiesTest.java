package com.utils;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;

class LoadPropertiesTest {
    private LoadProperties loadProperties;

    @BeforeEach
    void setUp() {
        loadProperties = new LoadProperties("config.properties");
        loadProperties.loadProperties();
        loadProperties.setProperties("testKey", "testValue");
    }

    @Test
    void getPropertyShouldReturnCorrectValue() {

        String value = loadProperties.getProperty("testKey");
        assertThat(value).isNotNull().isEqualTo("testValue");
    }

    @Test
    void loadPropertiesKeyNotExist() {
        String value = loadProperties.getProperty("test");
        assertThat(value).isNull();
    }
}
package com.utils.ddl;

import com.connection.PooledDataSource;
import com.utils.LoadProperties;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.powermock.api.mockito.PowerMockito;
import static org.powermock.api.mockito.PowerMockito.whenNew;
import org.mockito.junit.jupiter.MockitoExtension;

import org.junit.jupiter.api.BeforeEach;
import org.mockito.Mock;
import org.slf4j.Logger;

import java.io.BufferedReader;
import java.sql.Connection;
import java.sql.Statement;

import static org.junit.jupiter.api.Assertions.*;
        import static org.mockito.Mockito.*;
@ExtendWith(MockitoExtension.class)
public class TableCreationExecutorTest {
    @Mock
    private PooledDataSource mockedDataSource;
    @Mock
    private Connection mockedConnection;
    @Mock
    private LoadProperties mockedProperties;
    @Mock
    private Logger mockedLogger;
    @Mock
    private BufferedReader mockedReader;
    @Mock
    private Statement mockedStatement;

    private DDLCreationExecutor tableCreationExecutor;

    @BeforeEach
    public void setUp() {
        when(mockedDataSource.getConnection()).thenReturn(mockedConnection);
        when(mockedLogger.getName()).thenReturn(DDLCreationExecutor.class.getName());
        tableCreationExecutor = new DDLCreationExecutor();
        tableCreationExecutor.pooledDataSource = mockedDataSource;
        tableCreationExecutor.dataConnection = mockedConnection;
        Logger LOGGER = mockedLogger;
    }

    @Test
    public void testCreateDDLTables_SuccessfulExecution() throws Exception {
        String sqlScript = "DDL_SCHEMA_SCRIPT";
        String operationName = "creating DDL tables";
        when(mockedProperties.getProperty("DDL_schema")).thenReturn(sqlScript);
        when(mockedConnection.createStatement()).thenReturn(mockedStatement);
        when(mockedStatement.execute(anyString())).thenReturn(true);
        when(mockedReader.readLine()).thenReturn("CREATE TABLE table1;", "CREATE TABLE table2;");
        whenNew(BufferedReader.class).withAnyArguments().thenReturn(mockedReader);

        boolean result = tableCreationExecutor.createDDLTables(mockedProperties);

        assertTrue(result);

    }

    @Test
    public void testCreateDDLTables_ExceptionThrown() throws Exception {
        String sqlScript = "DDL_SCHEMA_SCRIPT";
        String operationName = "creating DDL tables";
        when(mockedProperties.getProperty("DDL_schema")).thenReturn(sqlScript);
        when(mockedConnection.createStatement()).thenThrow(new RuntimeException("Error executing SQL statement"));
        whenNew(BufferedReader.class).withAnyArguments().thenReturn(mockedReader);

        boolean result = tableCreationExecutor.createDDLTables(mockedProperties);

        assertFalse(result);
        verify(mockedLogger).error("Error {}: {} {}", operationName, "Error executing SQL statement", any(RuntimeException.class));
        verify(mockedConnection).close();
        verify(mockedReader).close();
    }

    @Test
    public void testAddIndex_SuccessfulExecution() throws Exception {
        String sqlScript = "DDL_INDEX_SCRIPT";
        String operationName = "adding indexes";
        when(mockedProperties.getProperty("DDL_add_index")).thenReturn(sqlScript);
        when(mockedConnection.createStatement()).thenReturn(mockedStatement);
        when(mockedStatement.execute(anyString())).thenReturn(true);
        when(mockedReader.readLine()).thenReturn("CREATE INDEX index1;", "CREATE INDEX index2;");

        PowerMockito.whenNew(BufferedReader.class).withAnyArguments().thenReturn(mockedReader);

        boolean result = tableCreationExecutor.addIndex(mockedProperties);

        assertTrue(result);

    }

}

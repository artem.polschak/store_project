DROP SCHEMA IF EXISTS public CASCADE;
CREATE SCHEMA public;

CREATE TABLE IF NOT EXISTS store (
                       id SERIAL PRIMARY KEY,
                       name VARCHAR(255) NOT NULL,
                       location VARCHAR(255) NOT NULL
);

CREATE TABLE IF NOT EXISTS product_type (
                      id SERIAL PRIMARY KEY,
                      name VARCHAR(255) NOT NULL
);


CREATE TABLE IF NOT EXISTS product (
                            id SERIAL PRIMARY KEY ,
                            name VARCHAR(255) NOT NULL,
                            product_type_id INT NOT NULL,
                            FOREIGN KEY (product_type_id) REFERENCES product_type(id)
);


CREATE TABLE IF NOT EXISTS remain (
                            amount INT NOT NULL,
                            product_id INT NOT NULL,
                            store_id INT NOT NULL,
                            FOREIGN KEY (product_id) REFERENCES product(id),
                            FOREIGN KEY (store_id) REFERENCES store(id)

);















